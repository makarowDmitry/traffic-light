package ru.mda.trafficlight;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    private ConstraintLayout mConstraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mConstraintLayout =  findViewById(R.id.constraintLayout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }


    public void onClickButton(View view) {
        switch (view.getId()) {

            case R.id.buttonRed:
                mConstraintLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.red));
                break;

            case R.id.buttonYel:
                mConstraintLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.yellow));
                break;

            case R.id.buttonGre:
                mConstraintLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
                break;

        }
    }





}
